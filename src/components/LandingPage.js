function LandingPage() {
  return (
    <div className="w-screen h-screen">
        <div className="w-full h-full bg-[#FAFAFA] flex justify-center items-center">
            Landing Page
        </div>
    </div>
  );
}

export default LandingPage;
